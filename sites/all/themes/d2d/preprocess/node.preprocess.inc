<?php


function d2d_preprocess_node(&$vars) {
  //For security reasons we do not want to expose the users user name.
  // This function provides an extra variable for use on the team member
  // page which uses the  'field_team_member_name' on the users profile
  if ($vars['node']->type == 'blog_posting') {
    //Load the user that made the post
    $post_owner = user_load($vars['node']->uid);
    $post_owner_name = field_get_items('user', $post_owner, 'field_team_member_name');
    //Extract the safe value
    $post_owner_name = $post_owner_name[0]['safe_value'];
    //Convert to lowercase for url
    $url = str_replace(' ','-', $post_owner_name);
    $url = strtolower($url);
    $url = 'about-us/' . $url;
    $link = l(t($post_owner_name), $url,array('attributes'=>array('class'=>'blog-poster')));
    //Format date
    $submitted = format_date($vars['node']->created, 'blog_post_date');

    $vars['blog_posted_on'] = '<div class="posted-by"><span class="posted-date">Posted on ' . $submitted
      . ' by </span><span class="poster">' . $link . '</span></div>';
  }
}