<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * d2d theme.
 */


/**
 * @param array $variables
 * @return string
 * Adds a delimiter between menu items
 */
function d2d_menu_link(array $variables)
{
  $delimiter = '';
  // Only add delimiter to the main menu
  if (strpos($variables['element']['#theme'], 'secondary_menu') !== FALSE || strpos($variables['element']['#theme'], 'privacy') !== FALSE) {
    // Only add delimiter if item is not the last one
    if (!in_array('last', $variables['element']['#attributes']['class'])) {
      $delimiter = '<li class="delimiter">|</li>';
    }
  }
  $element = $variables['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Add delimiter just before the closing </li> tag
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . $delimiter . "</li>\n";
}

function d2d_menu_alter() {

}


function d2d_page_alter(&$page) {
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == 'holidays') {
      //Add Holiday dates to page
      $markup = views_embed_view('holiday_dates', 'holiday_dates_block', $node->nid);
      $page['sidebar_second']['holiday_dates'] = array(
        '#markup' => $markup,
        '#weight' => -10,
      );
      $page['sidebar_second']['#sorted'] = false;
      //Add gallery to page
      $markup = views_embed_view('holiday_gallery', 'holiday_gallery_block', $node->nid);
      $page['content']['gallery'] = array(
        '#markup' => $markup,
        '#weight' => -10,
      );
      $page['content']['#sorted'] = false;
    }
    if ($node->nid == '218') {
      $markup = '';
      $markup .= '<div class"connect-contact"><label class="connect-label">' . t('Or connect with us on:') . '</label></div>';
      $markup .= '<div class="block--views-social-icons-block-2">' . $page['header']['views_social_icons-block']['#markup'] . '</div>';
      $page['content']['social'] = array(
        '#markup' => $markup,
        '#weight' => 50,
      );
      $page['content']['#sorted'] = false;
    }

  }
  if (drupal_is_front_page()) {
    //Relocate Contact form to main content region

    $webform = $page['sidebar_second']['webform_client-block-217'];
    $page['content']['webform_client-block-217'] = $webform;
    $page['content']['webform_client-block-217']['#weight'] = 100;
    unset($page['sidebar_second']);
    $page['content']['#sorted'] = false;
  }
}