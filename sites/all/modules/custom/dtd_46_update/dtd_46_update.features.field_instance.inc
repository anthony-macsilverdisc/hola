<?php
/**
 * @file
 * dtd_46_update.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function dtd_46_update_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_holiday_dates_group-field_duration'
  $field_instances['field_collection_item-field_holiday_dates_group-field_duration'] = array(
    'bundle' => 'field_holiday_dates_group',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_duration',
    'label' => 'Duration',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Duration');

  return $field_instances;
}
