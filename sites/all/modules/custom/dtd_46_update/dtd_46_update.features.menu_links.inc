<?php
/**
 * @file
 * dtd_46_update.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dtd_46_update_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_our-holidays:our-holidays
  $menu_links['main-menu_our-holidays:our-holidays'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'our-holidays',
    'router_path' => 'our-holidays',
    'link_title' => 'Our Holidays',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_our-holidays:our-holidays',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Our Holidays');


  return $menu_links;
}
