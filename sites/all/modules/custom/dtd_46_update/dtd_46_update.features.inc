<?php
/**
 * @file
 * dtd_46_update.features.inc
 */

/**
 * Implements hook_views_api().
 */
function dtd_46_update_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function dtd_46_update_image_default_styles() {
  $styles = array();

  // Exported image style: product_listing_4x.
  $styles['product_listing_4x'] = array(
    'name' => 'product_listing_4x',
    'label' => 'Product Listing 4x',
    'effects' => array(
      20 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 290,
          'height' => 290,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
