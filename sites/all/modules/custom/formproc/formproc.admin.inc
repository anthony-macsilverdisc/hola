<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the formproc module.
 */

/**
 * Form builder. Configure formproc.
 * 
 * @ingroup forms
 * @see system_settings_form()
 */

function formproc_admin_settings() {
	
	$form['formproc_endpoint']= array (
			'#type' 		=> 'textfield',
			'#title' 		=> t('Endpoing URL where form data is POSTed to'),
			'#description' 	=> t('When you submit a form, the field contents will be POSTed to this URL.'),
			'#size' 		=> 120,
			'#maxlength' 	=> 240,
			'#default_value'=> variable_get('formproc_endpoint', 'http://fp.silverdisc.cl/processForm.php'),
	);
	$form['formproc_node_ids_to_post']= array (
			'#type' 		=> 'textfield',
			'#title' 		=> t('Node IDs of the webforms to POST to Form Processor'),
			'#description' 	=> t('Type a comma-separated list of the node ids of the webforms whose data you ' .
								 'want posted to Form Procsesor. These forms need to comply with Form Processor ' . 
								 'guidelines available from the module help text.'),
			'#size' 		=> 120,
			'#maxlength' 	=> 240,
			'#default_value'=> variable_get('formproc_node_ids_to_post', ''),
	);
	$form['formproc_minimum_fields']= array (
			'#type' 		=> 'textfield',
			'#title' 		=> t('Fields the form must contain'),
			'#description' 	=> t('Type a comma-separated list of the field keys that are necessary for ' .
								 'forms to be POSTable to Form Processor. DO NOT USE SPACES.'),
			'#size' 		=> 120,
			'#maxlength' 	=> 240,
			'#default_value'=> variable_get('formproc_minimum_fields', 'contact, phone, email, siteId'),
	);
	
	
	return system_settings_form($form);
}